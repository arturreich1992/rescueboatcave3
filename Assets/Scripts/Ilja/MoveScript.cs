﻿using UnityEngine;

public class MoveScript: MonoBehaviour
{
    public float maxMoveSpeed = 10;
    public float maxRotateSpeed = 2;
    public float maxSurfaceSpeed = 4;

    //Gibt an wie stark das Wasser die Bewegungen des Ubootes ausbremst
    public float dampeningFactor = 0.99f;




    public float moveSpeed = 0;
    public float moveAcceleration = 0;

    public float rotateSpeed = 0;
    public float rotateAcceleration = 0;

    public float surfaceSpeed = 0;
    public float surfaceAcceleration = 0;

    private float delta = 0;
    

    void Start()
    {
        Screen.lockCursor = true;
    }

    void Update()
    {
        //zeit seit dem letztem frame bekommen
        delta = Time.deltaTime;

        //rotieren
        rotate(delta);      

        //auftauchen/abtauchen
        surface(delta);     

        //bewegen
        move(delta);
    }

    /*
     *bewegt das uboot
     */
    private void move(float delta)
    {
        //Beschleunigung und die neue Geschwindigkeit zum Bewegen daraus berechnen
        getMoveAcceleration();
        computeMoveSpeed(delta);

        //Und dann einfach das Uboot bewegen
        transform.position += transform.forward * moveSpeed * delta;
    }

    /*
     *rotiert das uboot
     */
    private void rotate(float delta)
    {        
        //Beschleunigung und die neue Geschwindigkeit zum Rotieren daraus berechnen
        getRotateAcceleration();
        computeRotateSpeed(delta);

        //Und dann eifnach das Uboot rotieren lassen
        transform.Rotate(Vector3.up * rotateSpeed * delta);
    }

    /*
     *lässt das uboot ab und auftauchen
     */
    private void surface(float delta)
    {
        //Beschleunigung und die neue Geschwindigkeit zum Auftauchen daraus berechnen
        getSurfaceAcceleration();
        computeSurfaceSpeed(delta);

        //Und dann einfach das Uboot bewegen
        transform.position += transform.up * surfaceSpeed * delta;
    }

    /*
     * findet die Beschleunigung zum Bewegen heraus
     */
    private void getMoveAcceleration()
    {
        //Wenn W oder S dann +10/-10 sonst 0 
        moveAcceleration = Input.GetKey(KeyCode.W) ? maxMoveSpeed : Input.GetKey(KeyCode.S) ? -maxMoveSpeed : 0;
    }

    /*
     * findet die Rotationsbeschleunigung heraus
     */
    private void getRotateAcceleration()
    {
        //Wenn A oder D dann +maxRotateSpeed/-maxRotateSpeed sonst 0 
        rotateAcceleration = Input.GetKey(KeyCode.D) ? maxRotateSpeed : Input.GetKey(KeyCode.A) ? -maxRotateSpeed : 0;
    }

    /*
     * findet die Auftauchbeschleunigung heraus
     */
    private void getSurfaceAcceleration()
    {
        //Wenn Q oder E dann +maxSurfaceSpeed/-maxSurfaceSpeed sonst 0 
        surfaceAcceleration = Input.GetKey(KeyCode.Q) ? maxSurfaceSpeed : Input.GetKey(KeyCode.E) ? -maxSurfaceSpeed : 0;
    }

    /*
     * berechnet die Geschwindigkeit zum Bewegen
     */
    private void computeMoveSpeed(float delta)
    {
        //Geschwindigkeit je nach Beschleunigung erhöhen
        moveSpeed += moveAcceleration * delta;

        //sollte sich aber in den Grenzen bewegen
        if (Mathf.Abs(moveSpeed) > maxMoveSpeed) { moveSpeed = Mathf.Sign(moveSpeed) * maxMoveSpeed; }

        //die Geschwindigkeit in jedem frame um 1% absenken lassen...triviale Implementierung geht auch besser vor allem stabiler und framerateunabhäginger!
        moveSpeed *= dampeningFactor;
    }

    /*
     * berechnet die Rotationsgeschwindigkeit
     */
    private void computeRotateSpeed(float delta)
    {
        //Geschwindigkeit je nach Beschleunigung erhöhen
        rotateSpeed += rotateAcceleration * delta;

        //sollte sich aber in den Grenzen bewegen
        if (Mathf.Abs(rotateSpeed) > maxRotateSpeed) { rotateSpeed = Mathf.Sign(rotateSpeed) * maxRotateSpeed; }

        //die Geschwindigkeit in jedem frame um 1% absenken lassen...triviale Implementierung geht auch besser vor allem stabiler und framerateunabhäginger!
        rotateSpeed *= dampeningFactor;
    }

    /*
     * berechnet die Auftauchgeschwindigkeit
     */
    private void computeSurfaceSpeed(float delta)
    {
        //Geschwindigkeit je nach Beschleunigung erhöhen
        surfaceSpeed += surfaceAcceleration * delta;

        //sollte sich aber in den Grenzen bewegen
        if (Mathf.Abs(surfaceSpeed) > maxSurfaceSpeed) { surfaceSpeed = Mathf.Sign(surfaceSpeed) * maxSurfaceSpeed; }

        //die Geschwindigkeit in jedem frame um 1% absenken lassen...triviale Implementierung geht auch besser vor allem stabiler und framerateunabhäginger!
        surfaceSpeed *= dampeningFactor;
    }
}