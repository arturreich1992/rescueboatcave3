﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderWaterMineExplosion : MonoBehaviour {
	
	public GameObject explosionEffect;
	public GameObject submarine;
    public submarineStat stat;
    AudioSource audioSource;
    public AudioClip explosionSound;

    public MinefieldSpwan spawnMinefield;
    //for testing
	private float explosionTime = 3f;

	// Use this for initialization
	void Start () {
        
        //declare submarine here
        submarine = GameObject.Find("Submarine");
        audioSource = gameObject.AddComponent<AudioSource>();
        stat = GameObject.FindObjectOfType<submarineStat>();
        spawnMinefield = GetComponentInParent<MinefieldSpwan>();
        //  explosionEffect = GameObject.Find("BigExplosionEffect");
    }
	
	// Update is called once per frame
	void Update () {
		explosionTime -= Time.deltaTime;
		if (explosionTime <= 0) {
			Explosion ();		
		}
	}
	void OnCollisionEnter(){
        StartCoroutine(Explosion());
        
    }

	IEnumerator Explosion(){
        audioSource.clip = explosionSound;
        audioSource.volume = 0.5f;
        audioSource.Play();
        var fx = Instantiate (explosionEffect, gameObject.transform.position, gameObject.transform.rotation);    
        fx.SetActive(true);
       
        yield return new WaitForSeconds(1);
    //    fx.SetActive(false);
      //  DestroyImmediate(fx,true);
       // DestroyImmediate(gameObject, true);
        spawnMinefield.respawn();
        stat.destroySubmarine();

        yield return null;
	}
}
