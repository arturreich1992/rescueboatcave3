﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collision : MonoBehaviour {

    submarineStat stat;

	// Use this for initialization
	void Start () {
        stat = GetComponentInParent<submarineStat>();
	}

    private void OnCollisionEnter(Collision collision)
    {
        stat.health = 0;
    }
}
