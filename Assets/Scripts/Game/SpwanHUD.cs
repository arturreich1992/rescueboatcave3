﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpwanHUD : MonoBehaviour {

    public GameObject hud;
    public GameObject ship;
    public submarineStat stat;
  //  private GameObject Hud_;
    public animator_helper ani_helper;

    // Use this for initialization
    void Start () {
		stat = GetComponentInParent<submarineStat>();
        ani_helper = GetComponentInParent<animator_helper>();
    }
	

    public void spawnHUD()
    {
            var Hud_ = GameObject.Instantiate(hud, transform.position, transform.rotation, transform.parent);
            Hud_.transform.Rotate(0, 90, 0);
            Hud_.transform.localPosition = new Vector3(1.5f, -0.46f, 6.6f);
            stat.searchHUD();
    }


    public void destroyHUD()
    {
        var oldhud = GameObject.Find("HUD(Clone)");
        stat.hudstat = null;
        Destroy(oldhud);
         oldhud = GameObject.Find("HUD(Clone)");
    }
}
