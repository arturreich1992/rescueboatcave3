﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinefieldSpwan : MonoBehaviour {

    public GameObject Minefields;
    public GameObject MfSpawn;
    private GameObject minef;
    

    // Use this for initialization
    void Start () {
        MfSpawn = GameObject.Find("MFSpawn");
        spawn();

	}

    public void spawn()
    {
        minef = Instantiate(Minefields, transform.position, transform.rotation, transform.parent);
        minef.transform.parent = MfSpawn.transform;
        minef.SetActive(true);
        minef.transform.position = new Vector3(2007, 310, 803);
        minef.transform.Rotate(0, -66.21f, 0);
    } 

    public void respawn()
    {
      
        Destroy(minef);
        spawn();
    }
}
