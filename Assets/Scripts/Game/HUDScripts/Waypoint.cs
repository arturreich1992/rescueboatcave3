﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

	public GameObject[] waypoints;
	public short waypointReachDistance;
	public GameObject centralGameObject;
    public pathfinder pathf;

	private int actualWaypoint;


    public Vector3 correctedCentralPosition;
    public Vector3 correctedWaypointPosition;
    public Vector3 targetDirection;
    float angle;
    float zeroangle;
    float agoangle;
    float shipAngle;

    void Start () {
        waypointReachDistance = 250;

        centralGameObject = GameObject.Find("Submarine");
        pathf = GetComponentInParent<pathfinder>();
        waypoints = pathf.getWaypoints();
        GetFirstWaypoint();

        //OrientateOnWaypoint();
    }

    void Update () {
		OrientateOnWaypoint();
		float distanceToWaypoint = CalculateWaypointDistance();
		if (distanceToWaypoint <= waypointReachDistance)
		{
			GetNextWaypoint();
		}
	}

    private void OrientateOnWaypoint()
    {
            correctedCentralPosition = centralGameObject.transform.position;
            //correctedCentralPosition.y =0;
          
            correctedWaypointPosition = waypoints[actualWaypoint].transform.position;
           // correctedWaypointPosition.y = 0;

            targetDirection = correctedWaypointPosition - correctedCentralPosition;
            
            angle = Vector3.Angle(targetDirection, (centralGameObject.transform.forward));

            if (Vector3.Angle(centralGameObject.transform.right,
                              correctedWaypointPosition - correctedCentralPosition) < 90)
            {
                Rotate_ship(-angle);
            }
            else
            {
                Rotate_ship(angle );
            }
    }


        private void Rotate_ship(float angle) 
	{
        Vector3 rotation = this.transform.localEulerAngles;
        rotation[2] = angle;
        //this.transform.localRotation.z = rotationYFromMaster;
        this.transform.localEulerAngles = rotation;
    }

	private float CalculateWaypointDistance()
	{
		float distance = Vector3.Distance(centralGameObject.transform.position,
		                                  waypoints[actualWaypoint].transform.position);
		Debug.Log("Distance: " + distance);
		return distance;
	}

	private GameObject GetFirstWaypoint()
	{
		actualWaypoint = 0;
		return waypoints[0];
	}

	public void GetNextWaypoint()
	{
		int waypointCount = waypoints.Length;
		if ((actualWaypoint + 1) < waypointCount)
		{
			actualWaypoint += 1;
		} else {
			// print "Final Waypoint" on HUD or the like
		}
	}
}
