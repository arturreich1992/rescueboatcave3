﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_rock : MonoBehaviour {

    public submarineStat stats;
    AudioSource audioSource;
    public AudioClip scrap; 

    // Use this for initialization
    void Start () {
        stats = GetComponentInParent<submarineStat>();
        audioSource = gameObject.AddComponent<AudioSource>();

    }

    private void OnCollisionEnter(Collision collision)
    {
        
        audioSource.clip = scrap;
        audioSource.Play();
        stats.halfdamage();
    }

   
}
