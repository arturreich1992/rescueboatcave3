﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animator_helper : MonoBehaviour
{

    Animator game;
    WiiController wii;

    public AudioClip seamineExplosion;

    public GameObject HUDSpawnPoint;
    public SpwanHUD spawnHUD;
    public engine_sound engine;
    public submarineStat stat;
    public submarineMovement move;
    AudioSource audioSource;


    public bool isstarted;
    public int activeHUD;
    public bool HudLock;

    // Use this for initialization
    void Start()
    {
        game = gameObject.GetComponent<Animator>();
        wii = WiiController.Instance;
        spawnHUD = GameObject.FindObjectOfType<SpwanHUD>();
        engine = GetComponentInChildren<engine_sound>();
        stat = GetComponentInChildren<submarineStat>();
        move = GameObject.FindObjectOfType<submarineMovement>();
        audioSource = GetComponent<AudioSource>();
        game.SetBool("autopilot", true);
        game.SetBool("startDrivingAI", false);


        HudLock = true;
        isstarted = false;
        activeHUD = 0;
    }

    // Update is called once per frame
    void Update()
    {
        forceRestart();

        if (!isstarted)
        {
            startGame();
        }

        if (isstarted)
        { 
            userinput();
            CheckHudSelection();
        }

    }

    private void startGame()
    {
    
            if (wii.buttonA() || wii.buttonB() || wii.buttonDown() || wii.buttonRight() || wii.buttonUp() || wii.buttonLeft() || Input.GetKey(KeyCode.Space))
            {
                game.SetTrigger("isAnyKeypressed");
              
                spawnHUD.spawnHUD();
                isstarted = true;
            }
           
        
    }

    void forceRestart()
    {
       
            if (wii.buttonHome() || Input.GetKeyDown(KeyCode.R))
            {
                stat.destroySubmarine();
            }
        
    }

    void userinput()
    {

        if (wii.buttonMinus() || wii.buttonPlus() || Input.GetKeyDown(KeyCode.X))
        {
            if (game.GetBool("autopilot"))
            {
                game.SetBool("autopilot", false);
            }
            if (!game.GetBool("autopilot"))
            {
                game.SetBool("autopilot", true);
            }

        }
        if (wii.buttonB() || Input.GetKeyDown(KeyCode.B))
        {
            game.SetTrigger("isBpressed");
        }

        // set the trigger to skip the tutorial
        if (Input.GetKeyDown(KeyCode.Y) || wii.buttonA()) { game.SetTrigger("skip"); }
    }

     private void CheckHudSelection()
    {
        if (HudLock == false)
        {
            if (activeHUD == 0)
            {
                activeHUD = 3;
            }
            if (wii != null)
            {
                if (wii.buttonB())
                {
                    var temp = activeHUD + 1;
                    if (temp % 4 == 0)
                    {
                        temp = 1;
                    }
                    activeHUD = temp;
                }
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                {
                    var temp = activeHUD + 1;
                    if (temp % 4 == 0)
                    {
                        temp = 1;
                    }
                    activeHUD = temp;
                }
            }
            
            
        }
    }

    public void restart()
    {
        //hier muss noch eine übergangscreen oder sowas eingebauen werden
        game.SetBool("autopilot", true);
        game.SetBool("startDrivingAI", false);
        Destroy(GameObject.Find("BigExplosionEffect(Clone)"));
        AudioSource audio = gameObject.GetComponentInChildren<AudioSource>();
        audio.Stop();
        engine.silent();
        stat.started = false;
        isstarted = false;
        HudLock = true;
        spawnHUD.destroyHUD();
        activeHUD = 0;
        move.reverse();
        StartCoroutine(moveAndResetSubmarine());
        game.Play("restart", 0, 0);
        
    }

    IEnumerator moveAndResetSubmarine()
    {
    
        yield return new WaitForSeconds(1);
        move.stopreverse();
    }


}
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animator_helper : MonoBehaviour
{

    Animator game;
    WiiController wii;

    public AudioClip seamineExplosion;

    public GameObject HUDSpawnPoint;
    public SpwanHUD spawnHUD;
    public engine_sound engine;
    public submarineStat stat;
    public submarineMovement move;
    AudioSource audioSource;


    public bool isstarted;
    public int activeHUD;
    public bool HudLock;

    // Use this for initialization
    void Start()
    {
        game = gameObject.GetComponent<Animator>();
        wii = WiiController.Instance;
        spawnHUD = GameObject.FindObjectOfType<SpwanHUD>();
        engine = GetComponentInChildren<engine_sound>();
        stat = GetComponentInChildren<submarineStat>();
        move = GameObject.FindObjectOfType<submarineMovement>();
        audioSource = GetComponent<AudioSource>();
        game.SetBool("autopilot", true);
        game.SetBool("startDrivingAI", false);


        HudLock = true;
        isstarted = false;
        activeHUD = 0;
    }

    // Update is called once per frame
    void Update()
    {
        forceRestart();

        if (!isstarted)
        {
            startGame();
        }

        if (isstarted)
        { 
            userinput();
            CheckHudSelection();
        }

    }

    private void startGame()
    {
        // when wii is there
        if (wii != null)
        {
            if (wii.buttonA() || wii.buttonB() || wii.buttonDown() || wii.buttonRight() || wii.buttonUp() || wii.buttonLeft() || Input.GetKey(KeyCode.Space))
            {
                game.SetTrigger("isAnyKeypressed");
              
                spawnHUD.spawnHUD();
                isstarted = true;
            }
           
        }
        else
        {
            if (Input.GetKey(KeyCode.Space))
            {
                game.SetTrigger("isAnyKeypressed");
               
                spawnHUD.spawnHUD();
                isstarted = true;
            }

          
        }
    }

    void forceRestart()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            stat.destroySubmarine();
        }

        if (wii != null) { 
            if (wii.buttonHome())
            {
                stat.destroySubmarine();
            }
        }

    
        
    }

    void userinput()
    {
        if (wii != null)
        {
            if (wii.buttonMinus() || wii.buttonPlus() || Input.GetKeyDown(KeyCode.X))
            {
                game.SetBool("autopilot", !game.GetBool("autopilot"));
                
            }
            if (wii.buttonB() || Input.GetKeyDown(KeyCode.B))
            {
                game.SetTrigger("isBpressed");
            }

            // set the trigger to skip the tutorial
            if (Input.GetKeyDown(KeyCode.Y) || wii.buttonA()) { game.SetTrigger("skip"); }

            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                game.SetTrigger("isShortcut");
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                if (game.GetBool("autopilot"))
                {
                    game.SetBool("autopilot", false);
                }
                else
                {
                    game.SetBool("autopilot", true);
                }

            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                game.SetTrigger("isBpressed");
            }

            if (Input.GetKeyDown(KeyCode.Y)) game.SetTrigger("skip");

            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                game.SetTrigger("isShortcut");

            }
        }
    }



    private void CheckHudSelection()
    {
        if (HudLock == false)
        {
            if (activeHUD == 0)
            {
                activeHUD = 3;
            }

            if (wii != null)
            {
                if (wii.buttonB() || Input.GetKeyDown(KeyCode.Alpha1))
                {
                    var temp = activeHUD + 1;
                    if (temp % 4 == 0)
                    {
                        temp = 1;
                    }
                    activeHUD = temp;
                }
            }
            else
            {
                if ( Input.GetKeyDown(KeyCode.Alpha1))
                {
                    var temp = activeHUD + 1;
                    if (temp % 4 == 0)
                    {
                        temp = 1;
                    }
                    activeHUD = temp;
                }
            }
        }
    }
   

    public void restart()
    {
        //hier muss noch eine übergangscreen oder sowas eingebauen werden
        game.SetBool("autopilot", true);
        game.SetBool("startDrivingAI", false);
        Destroy(GameObject.Find("BigExplosionEffect(Clone)"));
        AudioSource audio = gameObject.GetComponentInChildren<AudioSource>();
        audio.Stop();
        engine.silent();
        stat.started = false;
        isstarted = false;
        HudLock = true;
        spawnHUD.destroyHUD();
        activeHUD = 0;
        move.reverse();
        StartCoroutine(moveAndResetSubmarine());
        game.Play("restart", 0, 0);
        
    }

    IEnumerator moveAndResetSubmarine()
    {
    
        yield return new WaitForSeconds(1);
        move.stopreverse();
    }


}
