﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class submarineMovement : MonoBehaviour
{
    submarineSteering steering;

    //Instanz
    private float lateralAcceleration;
    private float angularAcceleration;
    private float verticalAcceleration;

    private float lateralForce = 50;
    private float verticalForce = 10;
    private float angularForce = 10;

    private Vector3 controls;
    private Rigidbody body;

    private bool steer;

    // Use this for initialization
    void Start()
    {
        steer = true;
        steering = GetComponent<submarineSteering>();
        body = GetComponent<Rigidbody>();
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        if (steer)
        {
            controls = steering.controls;

            //Integrate accelerations
            angularAcceleration = 0.95f * angularAcceleration + 0.05f * controls.x;
            verticalAcceleration = 0.9f * verticalAcceleration + 0.1f * controls.y;
            lateralAcceleration = 0.9f * lateralAcceleration + 0.1f * controls.z;
        }
  
            body.AddRelativeForce(Vector3.forward * lateralAcceleration * lateralForce);
            body.AddRelativeForce(Vector3.up * verticalAcceleration * verticalForce);
            body.AddRelativeTorque(Vector3.up * angularAcceleration * angularForce);

            //Autoupright ... submarine stays upright and dont rotate on the z and x 
            body.AddForceAtPosition(Vector3.up * 100, transform.TransformPoint(Vector3.up * 10));
            body.AddForceAtPosition(Vector3.down * 100, transform.TransformPoint(Vector3.down * 10));

        

    }

    public void reverse()
    {
        steer = false;
        verticalAcceleration = verticalAcceleration * -1 * 100;
        lateralAcceleration = lateralAcceleration * -1 * 100;
    }
    public void reverseSlow()
    {
        steer = false;
        verticalAcceleration = verticalAcceleration * -1 * 10;
        lateralAcceleration = lateralAcceleration * -1 * 10;
    }

    public void stopreverse()
    {
        angularAcceleration = 0;
        verticalAcceleration = 0;
        lateralAcceleration = 0;
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.transform.SetPositionAndRotation(new Vector3(2164, 240, 479), new Quaternion(0, 0, 0, 1));
        steer = true;
    }
}


