﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(submarineStat))]
// decides if the player can steer or the AI
public class submarineSteering : MonoBehaviour
{
    submarineStat stat;
    public animator_helper ani_helper;
    AI_control aicontrol;
    Animator game;
    WiiController wii;

    public Vector3 controls = Vector3.zero;
  

    // Use this for initialization
    void Start()
    {
        stat = GetComponentInParent<submarineStat>();
        ani_helper = GetComponentInParent<animator_helper>();
        aicontrol = GetComponent<AI_control>();
        game = gameObject.GetComponentInParent<Animator>();
        controls = Vector3.zero;
        wii = WiiController.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (!game.GetBool("autopilot"))
        { 
            controls = Vector3.zero;
            if (Input.GetKey(KeyCode.LeftArrow)) controls += Vector3.left;
            if (Input.GetKey(KeyCode.RightArrow)) controls += Vector3.right;

            if (Input.GetKey(KeyCode.UpArrow)) controls += Vector3.forward;
            if (Input.GetKey(KeyCode.DownArrow)) controls += Vector3.back;

            //Player steers 
            if (wii != null)
            {
                //Player steers 
                if (wii.holdButtonLeft() || Input.GetKey(KeyCode.LeftArrow)) controls += Vector3.left;
                if (wii.holdButtonRight() || Input.GetKey(KeyCode.RightArrow)) controls += Vector3.right;

                if (wii.holdButtonUp() || Input.GetKey(KeyCode.UpArrow)) controls += Vector3.forward;
                if (wii.holdButtonDown() || Input.GetKey(KeyCode.DownArrow)) controls += Vector3.back;
            }
        }
        // Ai can steer 
        else 
        {

            controls = aicontrol.controls;
        }


    }

   
}
