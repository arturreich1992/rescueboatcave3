﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class engine_sound : MonoBehaviour
{
 
    public AudioClip StartingSound;
    public AudioClip RunningSound;
    public AudioClip StoppingSound;

    submarineSteering steering;
    Vector3 acceleration;

    public AudioSource Source;
    SubmarineMovement MovementScript;
    float Pitch;
    bool isidle;

    // Use this for initialization
    void Start()
    {
        MovementScript = GetComponent<SubmarineMovement>();
        steering = GetComponent<submarineSteering>();
     

        Source = gameObject.AddComponent<AudioSource>();
        Source.clip = StartingSound;
        isidle = true;
        Source.volume = 0.4f;
        

    }

    // Update is called once per frame
    void Update()
    {
        Pitch = 0.9f * Pitch + 0.10f * (steering.controls.magnitude * 100f) ;
        
        Source.pitch = Pitch * 0.002f + 1f;
        if (Source.isPlaying == false && !isidle)
        {
            Source.clip = RunningSound;
            Source.loop = true;
            Source.Play();
        }

    }

    public void silent()
    {
        isidle = true;
        Source.Stop();
    }


    public void starting_engine()
    {
        Source.Play();
        Pitch = 1;
        isidle = false;
 
    }


    public void stop_enginge()
    {
        Source.clip = StoppingSound;
        Source.Play();
        Source.loop = false;
    }
}
