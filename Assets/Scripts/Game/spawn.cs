﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{

    public GameObject caveplayer;
    
    public GameObject flycam;
    public bool flycamOn = true;


    // Use this for initialization
    void Awake()
    {

#if UNITY_EDITOR 

        if (flycamOn)
        {
           var cam=  GameObject.Instantiate(flycam, transform.position, transform.rotation, transform.parent);
            cam.layer = 9;
            cam.SetActive(true);
            cam.transform.Translate(Vector3.up * 1);
            cam.transform.Rotate(0,180,0);
        }
        else
        {

           var player = GameObject.Instantiate(caveplayer, transform.position, transform.rotation, transform.parent);
            player.layer = 9;
            player.GetComponentInChildren<CamManager>().useCameraTestData = true;
        }
#else
                var player = GameObject.Instantiate(caveplayer,transform.position, transform.rotation,transform.parent);
                 player.layer = 9;
                player.GetComponentInChildren<CamManager>().useCameraTestData = false;
           
#endif

        GameObject.Destroy(gameObject);
    }
}
