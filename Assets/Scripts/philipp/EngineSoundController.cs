﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineSoundController : MonoBehaviour {

    public GameObject Vehicle; /** Fahrzeug, an das das Steuerungsscript angehängt ist */
    public AudioClip StartingSound; 
    public AudioClip RunningSound;

    AudioSource Source;
    SubmarineMovement MovementScript;
    float Pitch;

	// Use this for initialization
	void Start () {
        MovementScript = Vehicle.GetComponent<SubmarineMovement>();

        Source = GetComponent<AudioSource>();
        Source.clip = StartingSound;
        Source.Play();
        Pitch = 1;
	}
	
	// Update is called once per frame
	void Update () {
        Pitch = MovementScript.rpm;
        Source.pitch = Pitch * 0.002f + 1f;
        if (Source.isPlaying == false)
        {
            Source.clip = RunningSound;
            Source.Play();
            Source.loop = true;
        }   
        
	}
}
