﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// decides if the player can steer or the AI
/*
public class submarine_steering_new : MonoBehaviour
{
    HUD_stat HUD;

    public Vector3 force = Vector3.zero;
    public Vector3 AIControls = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        HUD = GetComponent<HUD_stat>();

    }

    // Update is called once per frame
    void Update()
    {
        WiiController wii = WiiController.Instance;
        force = Vector3.zero;

        // or autosteering
        if (HUD.HUDContext == 2)
        {
            //Player steers 
            if (wii != null)
            {
                if (wii.holdButtonLeft() || wii.holdButtonRight() || wii.holdButtonUp() || wii.holdButtonDown())
                {
                    force += Vector3.forward;
                }
            }

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
            {
                force += Vector3.forward;
            }
            
        }
        
    }
}
*/