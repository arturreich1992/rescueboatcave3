﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SubmarineMovement : MonoBehaviour
{

    private CharacterController player;

    private InputHandling inputHandling;

    /*
     * Mouse Stuff
     */
    public bool allowMouse = false;
    private float verticalRotationSpeed = -5f;
    private float horizontalRotationSpeed = 5f;

    /*
     * Hud Stuff
     */
    const int HUD_STANDBY = 0;
    const int HUD_STATUS = 1;
    const int HUD_DISCOVERY = 2;
    const int HUD_NAVIGATION = 3;

    public int activeHud = 0;

    /*
     * Ship Stuff
     */
    public float status = 0;

    public float air = 100;
    const float AIR_LOSS_FACTOR = 0.5f;

    public float rpm = 0;

    public bool engineIsOn = true;

    public bool lightInsideIsOn = false;
    public bool lightOutsideIsOn = false;


    /**
     * need?
     */
    private float huds;
    private float isMoving;
    private float canMove;
    private float Buttons;
    private float ButtonIsActive;
    private float pos;
    private float direction;


    /*
     * forward backward
     */
    public float currentSpeed = 0;
    private bool backwards = false;
    public bool isDriving = false;

    private float acceleration = 20.0f;
    private float backwardsAcceleration = 10.0f;
    private float breakingForce = 12f;

    const float ACTIVE_BREAKING_FORCE = 2f;
    const float MAX_SPEED = 20;
    const float MIN_SPEED = -10;

    /*
     * left right
     */
    public float currentTurningSpeed = 0;
    private bool left = false;
    public bool isTurning = false;

    private float turnAcceleration = 1.5f;
    private float turningBreakingForce = 1.5f;
    const float MAX_TURN_SPEED = 0.7f;
    const float MIN_TURN_SPEED = -0.7f;

    /*
     * up down
     */
    public float currentSurfacingSpeed = 0;
    private bool up = false;
    public bool isSurfacing = false;

    private float surfacingAcceleration = 8f;
    private float surfacingBreakingForce = 4f;

    const float MAX_SURFACING_SPEED = 5f;
    const float MIN_SURFACING_SPEED = -5f;

    /*
     * 
     */
    private bool hit = false;
    

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<CharacterController>();
        inputHandling = InputHandling.Instance;
    }

    public void Awake()
    {
        
    }

    public void SetActiveHud(int hud)
    {
        this.activeHud = hud;
    }

    // Update is called once per frame
    void Update()
    {
        
        AirControll();

        if (engineIsOn)
        {
            CheckControlls();
        }

        ForwardBackward();
        LeftRight();
        UpDown();

        CalcRPM();

        SyncPlayerPosition();
    }

    void SyncPlayerPosition()
    {
        player.transform.position = transform.position;
    }

    void AirControll()
    {
        air -= Time.deltaTime * AIR_LOSS_FACTOR;
    }

    public void CheckControlls()
    {
        // mouse
        if (allowMouse)
        {
            this.transform.Rotate(verticalRotationSpeed * Input.GetAxis("Mouse Y"), 0, 0);
            this.transform.Rotate(0, horizontalRotationSpeed * Input.GetAxis("Mouse X"), 0, Space.World);
        }
        

        ControllsForwardBackward();

        ControllsLeftRight();

        ControllsUpDown();
    }

    void ControllsForwardBackward()
    {
        if (inputHandling.KeyForwardIsPressed)
        {
            backwards = false;
            isDriving = true;
        }
        else if (inputHandling.KeyBackwardIsPressed)
        {
            backwards = true;
            isDriving = true;
        }
        else
        {
            backwards = false;
            isDriving = false;
        }
    }

    void ControllsLeftRight()
    {
        if (inputHandling.KeyTurnLeftIsPressed)
        {
            left = true;
            isTurning = true;
        }
        else if (inputHandling.KeyTurnRightIsPressed)
        {
            left = false;
            isTurning = true;
        }
        else
        {
            left = false;
            isTurning = false;
        }
    }

    void ControllsUpDown()
    {

        if (inputHandling.KeySurfacingIsPressed)
        {
            up = true;
            isSurfacing = true;
        }
        else if (inputHandling.KeyDiveIsPressed)
        {
            up = false;
            isSurfacing = true;
        }
        else
        {
            up = false;
            isSurfacing = false;
        }
    }

    void ForwardBackward()
    {

        float a = 0; // Beschleunigung 

        if (isDriving)
        {
            if (backwards)
            {
                if (currentSpeed > 0)
                {
                    a = -acceleration * ACTIVE_BREAKING_FORCE; // breaking
                }
                else
                {
                    a = -backwardsAcceleration; // backwarts
                }
            }
            else
            {
                a = acceleration;
            }
        }

        /*
         * speed
         */

        currentSpeed = currentSpeed + Time.deltaTime * a;

        if (!isDriving)
        {
            if (currentSpeed < -0.1)
            {
                currentSpeed += Time.deltaTime * breakingForce;
            }
            else if (currentSpeed > 0.1)
            {
                currentSpeed -= Time.deltaTime * breakingForce;
            }
        }

        if (currentSpeed > -0.1 && currentSpeed < 0.1)
        {
            currentSpeed = 0;
        }

        if (currentSpeed > MAX_SPEED)
        {
            currentSpeed = MAX_SPEED;
        }
        if (currentSpeed < MIN_SPEED)
        {
            currentSpeed = MIN_SPEED;
        }

        // Input.GetAxis("Horizontal") // -1 -> 1
        //moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 moveDirection = Vector3.zero;
        moveDirection = new Vector3(0, 0, 1);
        moveDirection = transform.TransformDirection(moveDirection);

        moveDirection.x *= currentSpeed;
        moveDirection.y = 0;
        moveDirection.z *= currentSpeed;

        transform.Translate(moveDirection * Time.deltaTime);
    }

    void LeftRight()
    {

        float a = 0; // Beschleunigung 

        if (isTurning)
        {
            if (left)
            {
                a = -turnAcceleration;
            }
            else
            {
                a = turnAcceleration;
            }
        }

        /*
         * speed
         */

        currentTurningSpeed = currentTurningSpeed + Time.deltaTime * a;

        if (!isTurning)
        {
            if (currentTurningSpeed < -0.01)
            {
                currentTurningSpeed += Time.deltaTime * turningBreakingForce;
            }
            else if (currentTurningSpeed > 0.01)
            {
                currentTurningSpeed -= Time.deltaTime * turningBreakingForce;
            }
        }

        if (currentTurningSpeed > -0.01 && currentTurningSpeed < 0.01)
        {
            currentTurningSpeed = 0;
        }

        if (currentTurningSpeed > MAX_TURN_SPEED)
        {
            currentTurningSpeed = MAX_TURN_SPEED;
        }
        if (currentTurningSpeed < MIN_TURN_SPEED)
        {
            currentTurningSpeed = MIN_TURN_SPEED;
        }

        this.transform.Rotate(0, currentTurningSpeed, 0, Space.World);
        player.transform.Rotate(0, currentTurningSpeed, 0, Space.World);
    }

    void UpDown()
    {

        float a = 0; // Beschleunigung 

        if (isSurfacing)
        {
            if (up)
            {
                a = surfacingAcceleration;
            }
            else
            {
                a = -surfacingAcceleration;
            }
        }

        /*
         * speed
         */

        currentSurfacingSpeed = currentSurfacingSpeed + Time.deltaTime * a;

        if (!isSurfacing)
        {
            if (currentSurfacingSpeed < -0.01)
            {
                currentSurfacingSpeed += Time.deltaTime * surfacingBreakingForce;
            }
            else if (currentSurfacingSpeed > 0.01)
            {
                currentSurfacingSpeed -= Time.deltaTime * surfacingBreakingForce;
            }
        }

        if (currentSurfacingSpeed > -0.01 && currentSurfacingSpeed < 0.01)
        {
            currentSurfacingSpeed = 0;
        }

        if (currentSurfacingSpeed > MAX_SURFACING_SPEED)
        {
            currentSurfacingSpeed = MAX_SURFACING_SPEED;
        }
        if (currentSurfacingSpeed < MIN_SURFACING_SPEED)
        {
            currentSurfacingSpeed = MIN_SURFACING_SPEED;
        }

        Vector3 moveDirection = new Vector3(0, 1, 0);
        moveDirection = transform.TransformDirection(moveDirection);

        moveDirection.x = 0;
        moveDirection.y *= currentSurfacingSpeed;
        moveDirection.z = 0;

        transform.Translate(moveDirection * Time.deltaTime);
    }

    void CalcRPM()
    {

        float rpm1 = 0;
        float rpm2 = 0;
        float rpm3 = 0;
        if (currentSpeed < 0)
        {
            rpm1 = currentSpeed / MIN_SPEED * 100;
        }
        else
        {
            rpm1 = currentSpeed / MAX_SPEED * 100;
        }

        if (currentTurningSpeed < 0)
        {
            rpm2 = currentTurningSpeed / MIN_TURN_SPEED * 100;
        }
        else
        {
            rpm2 = currentTurningSpeed / MAX_TURN_SPEED * 100;
        }

        if (currentSurfacingSpeed < 0)
        {
            rpm3 = currentSurfacingSpeed / MIN_SURFACING_SPEED * 100;
        }
        else
        {
            rpm3 = currentSurfacingSpeed / MAX_SURFACING_SPEED * 100;
        }

        rpm = Mathf.Max(rpm1, rpm2, rpm3);
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (!hit)
        {
            hit = true;
            backwards = !backwards;
            currentSpeed /= -2;

            currentTurningSpeed = 0;
            isTurning = false;

            up = !up;
            currentSurfacingSpeed /= -2;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        hit = false;
    }
}
