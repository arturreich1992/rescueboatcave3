﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandling : MonoBehaviour
{

    public bool KeyForwardIsPressed = false;
    public bool KeyBackwardIsPressed = false;

    public bool KeyTurnLeftIsPressed = false;
    public bool KeyTurnRightIsPressed = false;

    public bool KeySurfacingIsPressed = false;
    public bool KeyDiveIsPressed = false;

    /*
     * Wii stuff
     */
    private WiiController wiiController;

    public bool wiiIsConnected = false;

    public static InputHandling Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {

        wiiController = WiiController.Instance;

        try
        {
            if (wiiController.WiiMote != null)
            {
                wiiIsConnected = true;
            }
        }
        catch (System.Exception)
        {
            wiiIsConnected = false;
        }

    }

    // Update is called once per frame
    void Update()
    {

        CheckMovement();

    }


    private void CheckMovement()
    {
        CheckForward();
        CheckBackward();
        CheckTurnLeft();
        CheckTurnRight();
        CheckSurfacing();
        CheckDive();
    }

    private void CheckForward()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonUp())
                {
                    KeyBackwardIsPressed = true;
                }
                else
                {
                    KeyBackwardIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.W))
        {
            KeyForwardIsPressed = true;
        }
        else
        {
            KeyForwardIsPressed = false;
        }

    }

    private void CheckBackward()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonDown())
                {
                    KeyForwardIsPressed = true;
                }
                else
                {
                    KeyForwardIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.S))
        {
            KeyBackwardIsPressed = true;
        }
        else
        {
            KeyBackwardIsPressed = false;
        }
    }

    private void CheckTurnLeft()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonLeft())
                {
                    KeyTurnLeftIsPressed = true;
                }
                else
                {
                    KeyTurnLeftIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.A))
        {
            KeyTurnLeftIsPressed = true;
        }
        else
        {
            KeyTurnLeftIsPressed = false;
        }
    }

    private void CheckTurnRight()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonRight())
                {
                    KeyTurnRightIsPressed = true;
                }
                else
                {
                    KeyTurnRightIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.D))
        {
            KeyTurnRightIsPressed = true;
        }
        else
        {
            KeyTurnRightIsPressed = false;
        }
    }

    private void CheckSurfacing()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonPlus())
                {
                    KeySurfacingIsPressed = true;
                }
                else
                {
                    KeySurfacingIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.Space))
        {
            KeySurfacingIsPressed = true;
        }
        else
        {
            KeySurfacingIsPressed = false;
        }
    }

    private void CheckDive()
    {
        try
        {
            if (wiiController.useWii)
            {
                if (wiiController.holdButtonMinus())
                {
                    KeyDiveIsPressed = true;
                }
                else
                {
                    KeyDiveIsPressed = false;
                }
            }
        }
        catch (System.Exception)
        {

        }

        if (Input.GetKey(KeyCode.C))
        {
            KeyDiveIsPressed = true;
        }
        else
        {
            KeyDiveIsPressed = false;
        }
    }

    public void controllerUpdate()
    {
        /*
        private GameObject player;
        Debug.Log("searching player");
        player = GameObject.Find("Player");

        Debug.Log(player.ToString());
        if (wiiController.holdButtonUp())
        {
            Vector3 direction = new Vector3(0, 0, 1);
            direction = player.transform.TransformDirection(direction);
            player.GetComponent<CharacterController>().Move(direction * this.defaultWalkVelocityMeterPerSec * Time.deltaTime);
        }

        if (wiiController.holdButtonDown())
        {
            Vector3 direction = new Vector3(0, 0, -1);
            direction = player.transform.TransformDirection(direction);
            player.GetComponent<CharacterController>().Move(direction * this.defaultWalkVelocityMeterPerSec * Time.deltaTime);
        }

        if (wiiController.holdButtonRight())
        {
            player.transform.Rotate(new Vector3(0, this.defaultTurnVelocityDegPerSec * Time.deltaTime, 0));
        }

        if (wiiController.holdButtonLeft())
        {
            player.transform.Rotate(new Vector3(0, -this.defaultTurnVelocityDegPerSec * Time.deltaTime, 0));
        }


        if (wiiController.buttonPlus())
        {
            Application.LoadLevel(Application.loadedLevel);
            //this.GetComponentInChildren<Skeleton>().Enabled = !this.GetComponentInChildren<Skeleton>().Enabled;
        }

        if (wiiController.buttonHome())
        {
            Application.Quit();
        }

        if (wiiController.buttonPlus())
        {
        }
        */
    }
}
