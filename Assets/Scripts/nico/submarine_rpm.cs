﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
[RequireComponent(typeof(submarine_steering_new))]
public class submarine_rpm : MonoBehaviour
{
    submarine_steering_new steering;

    //Instanz
    private float lateral_speed;
    private float lateral_acceleration;

    private float lateral_force = 50;

    public float rpm = 0;
    public bool engineIsOn = true;


    // Use this for initialization
    void Start()
    {
        steering = GetComponent<submarine_steering_new>();
    }



    // Update is called once per frame
    void FixedUpdate()
    {

        Vector3 force = steering.force;

        //Integrate accelerations
        lateral_acceleration = 0.9f * lateral_acceleration + 0.1f * force.x;

        var body = GetComponent<Rigidbody>();
        body.AddRelativeForce(Vector3.forward * lateral_acceleration * lateral_force);


        //RPM Anbindung muss noch hier rein 
        // werte zwischen 0-100  ... 0 ist aus .. wert für motorlaufen 
        rpm = lateral_acceleration * lateral_force;
        if (rpm > 100)
        {
            rpm = 100;
        }

        if (engineIsOn && rpm <= 0)
        {
            rpm = 10;
        }

        if (!engineIsOn && rpm <= 0)
        {
            rpm = 0;
        }
    }
}


    */